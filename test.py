import operator

arr={};
with open('text2.txt','r') as f:
    for line in f:
        if '--' in line:
            line = line.replace("--"," ")
        for phrase in line.split('.'):
            i = 0

            ###nettoyage des articles/déterminants
            for word in phrase.split():
                if word in ('the','and','you','a','an','I','that','this','of','not','but','he','to','is','me','his','her','it','in','by','at','our','he','so','on','be','him','no','had','He','was'):
                    phrase = phrase.replace(" "+word,"");

            for word in phrase.split():
                #### nettoyage des ponctuations
                if ',' in word:
                    word = word.replace(",", "")
                if '?' in word:
                    word = word.replace("?", "")
                if '!' in word:
                    word = word.replace("!", "")
                if '"' in word:
                    word = word.replace('"', "")
                if '.' in word:
                    word = word.replace(".", "")
                if ';' in word:
                    word = word.replace(";", "")
                if '(' in word:
                    word = word.replace("(", "")
                if ')' in word:
                    word = word.replace("(", "")

                ### remplissage du dictionnaire avec des clés du style [mot -- mot_qui_suit]

                if ( (i+1) < len(phrase.split()) ):
                    new_key = word + " " + phrase.split()[i+1]
                    if new_key not in arr:
                        arr[new_key] = 1
                    else:
                        arr[new_key] += 1
                if ( (i+2) < len(phrase.split()) ):
                    new_key = word + " " + phrase.split()[i+2]
                    if new_key not in arr:
                        arr[new_key] = 1
                    else:
                        arr[new_key] += 1
                if ( (i+3) < len(phrase.split()) ):
                    new_key = word + " " + phrase.split()[i+3]
                    if new_key not in arr:
                        arr[new_key] = 1
                    else:
                        arr[new_key] += 1
                i+=1

sorted_arr = sorted(arr.items(), key=operator.itemgetter(1))
arr = dict(sorted_arr)
print(arr)